import { Localizer } from "./localizer.js";
import { State } from "./uploadstatusconstants.js";

/* Use globals automatically defined by ids in progress.html */
/* globals buttonClear, status_table, no_uploads, templates, spanButtonClear */

const port = browser.runtime.connect();
port.onMessage.addListener(updateStatusDisplay);

window.addEventListener("load", () => {
    Localizer.localizeLabels();

    buttonClear.addEventListener("click", handleClearButton);
});

/**
 * Send a message to background page that removes all UploadStatus items that have errors or generated passwords
 */
function handleClearButton() {
    port.postMessage("clearDone");
}

/**
 * Handler for the message coming from allUploadStatus on changes
 * @param {StatusMap} uploads The message. It contains allUploadStatus
 */
function updateStatusDisplay(uploads) {
    /* Remove table rows that don't correspond to an active upload */
    status_table.querySelectorAll("tr").forEach(row => {
        if (undefined === uploads.get(row.id)) {
            row.remove();
        }
    });
    /* Update existing or create new rows */
    for (const [Id, uploadStatus] of uploads) {
        let row = status_table.querySelector(`#${Id}`);
        if (!row) {
            row = templates.querySelector(".row").cloneNode(true);
            row.id = Id;
            status_table.appendChild(row);
        }
        updateRow(row, uploadStatus);
    }
    spanButtonClear.hidden = !hasInformation(uploads);
    no_uploads.hidden = (0 !== uploads.size);
}

/**
 * Fill one row in the status table with data and (un-)hide the corresponding cells
 * @param {HTMLTableRowElement} row An existing row in the status table
 * @param {UploadStatus} uploadStatus The UploadStatus object to show in that row
 */
function updateRow(row, uploadStatus) {
    const status_cell = row.querySelector(".status");

    row.querySelector(".filename").textContent = uploadStatus.filename;

    if (uploadStatus.error) {
        status_cell.classList.add('error');
        status_cell.textContent =
            browser.i18n.getMessage('status_error',
                browser.i18n.getMessage(`status_${uploadStatus.status}`));
        _showProgress(false);
    } else if (uploadStatus.status === State.DONE && !!uploadStatus.download_password) {
        status_cell.textContent = uploadStatus.download_password;
        row.querySelector(".td_copy").hidden = false;
        row.querySelector(".copy").addEventListener('click', () => navigator.clipboard.writeText(uploadStatus.download_password));
        _showProgress(false);
    } else if (uploadStatus.status === State.UPLOADING && uploadStatus.progress >= 0.0) {
        row.querySelector(".progress").value = uploadStatus.progress;
        _showProgress(true);
    } else {
        status_cell.textContent = uploadStatus.status;
        _showProgress(false);
    }

    /**
     * Alternatively hide the progress bar or the status text field
     * @param {boolean} show Show the progress bar?
     */
    function _showProgress(show) {
        status_cell.hidden = show;
        row.querySelector(".progress").hidden = !show;
    }
}

/**
 * Is one of the status in error state or has a generated password to show?
 * @param {Map} map The StatusMap object to inspect
 * @returns {boolean} Is it?
 */
function hasInformation(map) {
    for (const [, status] of map) {
        if (!!status.error || status.status === State.DONE && !!status.download_password) {
            return true;
        }
    }
    return false;
}
