export class Localizer {

    /**
     * In a content script put the localizes texts into all elements that have a "data-message" attribute
     */
    static async localizeLabels() {
        document.querySelectorAll("[data-message]")
            .forEach(element => {
                const msg = browser.i18n.getMessage(element.dataset.message);
                element.textContent = !!msg ? msg : `__MSG_${element.dataset.message}__`;
            });
    }

    /**
     * Find the localized string if the manifest value is in ___MSG_ format
     * @param {string} manifestString The string from manifest.json
     * @returns {string} The corresponding localized string or the string itself if it's not in __MSG format or no localization available
     */
    static localizeManifestString(manifestString) {
        return manifestString.replace(
            /^__MSG_([@\w]+)__$/, (matched, key) => {
                return browser.i18n.getMessage(key) || matched;
            });
    }
}
