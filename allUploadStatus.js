import { State } from "./uploadstatusconstants.js";

class StatusMap extends Map {
    constructor() {
        super();
        this.port = null;
        browser.runtime.onConnect.addListener(this.makeConnectHandler());
    }
    
    /**
     * Update the Badge on the compose action button and the status popup, if it's open
     */
    async update() {
        /* Show the number of current uploads in the badge on the compose action button */
        messenger.composeAction.setBadgeText({ text: !this.size ? null : this.size.toString(), });
        /* Update the popup if it's open */
        if (this.port) {
            this.port.postMessage(this);
        }
    }

    /**
     * Add an UploadStatus to the map
     * @param {string} accountId 
     * @param {UploadStatus} uploadStatus 
     */
    async set(accountId, uploadStatus) {
        super.set(accountId, uploadStatus);
        this.update();
        return this;
    }

    /**
     * Remove one upload from the status map
     * @param {string} uploadId The id of the upload created in background.js
     */
    async delete(uploadId) {
        const r = super.delete(uploadId);
        this.update();
        return r;
    }

    /**
     * Remove all Status objects from attachmentStatus that are in error state
     */
    async clearDone() {
        this.forEach((uploadStatus, uploadId) => {
            if (true === uploadStatus.error || State.DONE === uploadStatus.status) {
                this.delete(uploadId);
            }
        });
    }

    /**
     * Event handler for port setup as a clojure to have access to "this"
     * @param {Port} p The messaging Port of the Addon
     */
    makeConnectHandler() {
        return p => {
            this.port = p;
            this.update();
            this.port.onDisconnect.addListener(() => this.port = null);
            this.port.onMessage.addListener(this.makeMsgDispatcher());
        };
    }

    /**
     * Dispatch a command received as a message. This returns a clojure to have access to "this"
     * @param {string} msg The command as received as a message
     */
    makeMsgDispatcher() {
        return msg => {
            switch (msg) {
                case "clearDone":
                    this.clearDone();
                    break;
                default:
                    throw ReferenceError('No handler for ' + msg);
            }
        };
    }
}

/* A StatusMap to keep track of all current uploads */
export const allUploadStatus = new StatusMap();
