import { Popup } from "./popup.js";
import { Account } from "../account/account.js";

// Defined in management.html as ids:
/* globals useNoDlPassword, oneDLPassword, downloadPassword */
/* globals expiryDays, useExpiry */

export class GenericFormHandler {

    /**
     * Create a new FormHandler object, stowing away pointers to the account and the form for later use
     * @param {Account} account The account to handle in this form
     * @param {HTMLFormElement} form The form
     */
    constructor(accountId, form) {
        this.account = new Account(accountId);
        this.form = form;
    }

    /**
     * Copy data from the account into the corresponding fields of the form
     */
    async copyDataFromAccount() {
        document.querySelectorAll("input")
            .forEach(inp => {
                if (inp.type === "checkbox" || inp.type === "radio") {
                    inp.checked = !!this.account[inp.id];
                } else if (this.account[inp.id]) {
                    inp.value = this.account[inp.id];
                }
            });
        this.enforceExpiry();
        this.enforceDLPassword();
    }

    /**
     * If the server requires download passwords, make the user set one
     */
    async enforceDLPassword() {
        if (!!this.account.forceDLPasswords) {
            useNoDlPassword.disabled = true;
            oneDLPassword.checked = useNoDlPassword.checked;
            downloadPassword.disabled = !oneDLPassword.checked;
            downloadPassword.required = oneDLPassword.checked;
        }
    }

    /**
     * If the server requires download expiry, make the user set it
     */
    async enforceExpiry() {
        if (!!this.account.forceExpiry) {
            useExpiry.disabled = true;
            useExpiry.checked = true;
            expiryDays.required = true;
            expiryDays.disabled = false;
        }
        if (!!this.account.maxExpiry) {
            expiryDays.max = this.account.maxExpiry;
            expiryDays.value = Math.min(parseInt(expiryDays.value), this.account.maxExpiry);
        }
    }

    /**
     * Load the account from storage and copy the data to the form
     */
    async loadDataFromAccount() {
        await this.account.load();
        this.copyDataFromAccount();
    }

    /**
     * Copy data from the form to the account
     */
    copyDataToAccount() {
        document.querySelectorAll("input")
            .forEach(inp => {
                if (inp.type === "checkbox" || inp.type === "radio") {
                    this.account[inp.id] = inp.checked;
                }
                else {
                    this.account[inp.id] = inp.value;
                }
            });
    }

    /**
     * @param {InputEvent} event The event that triggered the call to this function
     */
    async handleSaveButton(event) {
        this.lookBusy(true);
        Popup.clear();
        this.form.querySelector('[type="submit"]').disabled = true;
        this.form.querySelector('[type="reset"]').disabled = true;
        await this.handleFormData();
        this.account.store();
        this.lookBusy(false);
        event.preventDefault();
    }

    /**
     * Reset form data to the state of the account object
     * @param {InputEvent} event The event that triggered the call to this function
     */
    async handleResetButton(event) {
        Popup.clear();
        this.copyDataFromAccount();
        this.updateHeader();
        this.form.querySelector('[type="submit"]').disabled = true;
        this.form.querySelector('[type="reset"]').disabled = true;
        event.preventDefault();
    }

    /**
     * Disable the input form and show the busy mouse pointer
     * @param {boolean} busy Is the form busy processing the data?
     */
    async lookBusy(busy = true) {
        if (busy) {
            document.querySelector("body").classList.add('busy');
        } else {
            document.querySelector("body").classList.remove('busy');
        }
        this.form.querySelector("fieldset").disabled = busy;
    }

    async handleFormData() {
        this.copyDataToAccount();
        await this.account.refresh_token();
        this.copyDataFromAccount();
    }

    /**
     * Enable the Save button only if he form data is valid and the Reset button if any input was changed
     */
    async updateButtonAvailability() {
        this.form.querySelector('[type="submit"]').disabled = !this.form.checkValidity();
        this.form.querySelector('[type="reset"]').disabled = false;
    }

    /**
     * Disable the input element if the checkbox/radio is not checked
     * @param {HTMLInputElement} input 
     * @param {HTMLInputElement} check A checkbox or a radio button
     */
    async setInputStateToCheckState(input, check) {
        input.disabled = !check.checked;
        input.required = !input.disabled;
        this.updateButtonAvailability();
    }
}
