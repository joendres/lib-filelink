import { allUploadStatus } from "./allUploadStatus.js";
import { State } from "./uploadstatusconstants.js";

export class UploadStatus {
    /**
     * @param {string} filename The name of the attachment that is uploaded
     */
    constructor(filename) {
        this.filename = filename;
        this.status = State.PREPARING;
        this.progress = -1.0;
    }

    /**
     * Set new status, possible values defined in _locales and progress.js
     * @param {string} status The new status from the State object
     */
    async setStatus(status) {
        this.status = status;
        allUploadStatus.update();
    }

    /**
     * Set the upload status to error state
     */
    async fail() {
        this.error = true;
        allUploadStatus.update();
    }

    /**
     * Update the upload progress
     * @param {number} progress Fraction of data already transferred as a float
     */
    async setProgress(progress) {
        this.progress = progress;
        allUploadStatus.update();
    }

    async setPassword(password) {
        this.download_password = password;
    }

    /**
     * Save the uploadRequest to later handle aborts
     * @param {XMLHttpRequest} uploadRequest 
     */
    async startRequest(uploadRequest) {
        this.uploadRequest = uploadRequest;
    }

    /**
     * When the upload is done, we don't need the Request any more
     */
    async endRequest() {
        delete (this.uploadRequest);
    }

    /**
     * Abort the upload
     */
    async abort() {
        if (this.uploadRequest && this.uploadRequest.abort) {
            this.uploadRequest.abort();
        }
    }
}
