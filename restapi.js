export class RestApi {
    /**
     * Call a REST endpoint and return the raw result
     * @param {Account} account 
     * @param {string} endpoint The url of the endpoint, relative to the account's base url
     * @param {string} method HTTP method
     * @param {Object<string,string>} parameters URL parameters to ths call
     * @param {?string} body The Body for POST/PUT
     * @param {Object<string,string>} moreHeaders Headers specific for this call, an object with the header names as keys
     * @returns {Promise<Response>} If the request fails, this returns a Ŕesponse with ok=false anyway.
    */
    static async doRawCall(account, endpoint, method = "GET", parameters = {}, body = null, moreHeaders = {}) {
        const url = account.restUrl();
        url.pathname += endpoint;
        for (const key in parameters) {
            url.searchParams.append(key, parameters[key]);
        }

        const headers = Object.assign(account.headers(), moreHeaders);
        const credentials = "omit";

        const init = {
            method,
            headers,
            credentials,
        };

        if (body && method.match("/^PUT|POST|PATCH$/i")) {
            init.body = body;
        }

        let response;
        try {
            response = await fetch(url, init);
        } catch (e) {
            response = Response.error();
        }
        return response;
    }

    /**
     * Call a REST endpoint and parse the response body as JSON
     * @param {Account} account 
     * @param {string} endpoint The url of the endpoint, relative to the account's base url
     * @param {string} method HTTP method
     * @param {Object<string,string>} parameters URL parameters to ths call
     * @param {string} body The Body for POST/PUT
     * @param {Object<string,string>} moreHeaders Headers specific for this call, an object with the header names as keys
     * @returns { Promise < Response >} The response with the parsed json data in the data property
    */
    static async doJsonCall(account, endpoint, method = "GET", parameters = {}, body = null, moreHeaders = {}) {
        const response = await this.doRawCall(account, endpoint, method, parameters, body, moreHeaders);
        if (response.ok) {
            try {
                response.data = await response.json();
            } catch (e) { }
        }
        return response;
    }

    /**
     * Call a REST endpoint and parse the response body as XML
     * @param {Account} account 
     * @param {string} endpoint The url of the endpoint, relative to the account's base url
     * @param {Object<string,string>} parameters URL parameters to ths call
     * @param {string} method HTTP method
     * @param {string} body The Body for POST/PUT
     * @param {Object<string,string>} moreHeaders Headers specific for this call, an object with the header names as keys
     * @returns { Promise < Response >} The response with the parsed XML Document in the data property
    */
    static async doXMLCall(account, endpoint, method = "GET", parameters = {}, body = null, moreHeaders = {}) {
        const response = await this.doRawCall(account, endpoint, method, parameters, body, moreHeaders);
        if (response.ok) {
            try {
                response.data = new DOMParser().parseFromString(await response.text(), 'application/xml');
            } catch (e) { }
        }
        return response;
    }
}
