/**
 * Generates a random string of ASCII characters between '!' (code 0x21) and '~' (code 0x7e) 
 * @param {number} minlength The minimum length of the generated password, default: 12
 * @param {string} excludes A string of characters to exclude, default:none
 * @param {function} meetsConstraints A callback that returns true if the given password meets the constraints, default: a lower case letter, an uppercase letter, a number and a special character
 * @throws {RangeError} If the constraints are not met within 4*minlength characters
 * @returns {string} A password of at least minlength and meeting the constraints given as callback
 */
 export function generatePassword(minlength = 12, excludes = "", meetsConstraints = lowerUpperNumberSpecial) {
    let password = "";
    while (password.length < minlength || !meetsConstraints(password)) {
        let c = String.fromCodePoint(0x21 + Math.floor(Math.random() * 94));

        while (!!excludes && excludes.includes(c)) {
            c = String.fromCodePoint(0x21 + Math.floor(Math.random() * 94));
        }
        password += c;
        if (password.length > 4 * minlength) {
            throw RangeError();
        }
    }
    return password;
}

/**
 * Check if the given password contains a lower case letter, an uppercase letter, a number and a special character
 * @param {string} password A string of ASCII characters
 * @returns {boolean} True if all the string contains all 4 types of characters, false otherwise
 */
function lowerUpperNumberSpecial(password) {
    return password.match(/\d/) &&
        password.match(/[-\\\]!"#$%&'()*+,./:;<=>?@\[^_`{|}~]/) &&
        password.match(/[A-Z]/) &&
        password.match(/[a-z]/);
}
