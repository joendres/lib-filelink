export const State = {
    DONE: "done",
    PREPARING: "preparing",
    MOVING: "moving",
    CHECK_SIZE: "size",
    CHECK_SPACE: "space",
    CREATE_LIBRARY: "library",
    CREATE_FOLDER: "folder",
    UPLOADING: "uploading",
    SHARING: "sharing",
};
