import { Account } from "../account/account.js";
import { Uploader } from "../uploader/uploader.js";
import { allUploadStatus } from "./allUploadStatus.js";

window.addEventListener("load", () => {
    /* Initialize all configured accounts when the Addon is loaded */
    browser.storage.local.get().then(
        allAccounts => {
            for (const accountId in allAccounts) {
                setupAccount(accountId);
            }
        });
});

/* Fired when a cloud file account of this add-on was deleted */
messenger.cloudFile.onAccountDeleted.addListener(accountId => {
    const acc = new Account(accountId);
    acc.deleteAccount();
});

/* Fired when a cloud file account of this add-on was created before the management page is opened */
messenger.cloudFile.onAccountAdded.addListener(async account => {
    const acc = new Account(account.id);
    acc.setupNewAccount();
    acc.store();
});

/* Fired when a file should be uploaded to the cloud file provider */
messenger.cloudFile.onFileUpload.addListener(async (account, fileInfo) => {
    const acc = new Account(account.id);
    await acc.load();
    const uploader = new Uploader(acc, fileInfo, makeUploadId(account, fileInfo.id));
    return uploader.uploadFile();
});

/* Fired when a file uploaded is aborted */
messenger.cloudFile.onFileUploadAbort.addListener(
    (account, fileId) => {
        const uploadStatus = allUploadStatus.get(makeUploadId(account, fileId));
        if (uploadStatus) {
            uploadStatus.abort();
        }
        allUploadStatus.delete(makeUploadId(account, fileId));
    });

/* Fired when a file previously uploaded should be deleted
   This Addon doesn't delete files because we want to reuse uploads. */
messenger.cloudFile.onFileDeleted.addListener(
    (account, fileId) => {
        allUploadStatus.delete(makeUploadId(account, fileId));
    });

/* This is called for every account on Thunderbird startup */
async function setupAccount(accountId) {
    const acc = new Account(accountId);
    await acc.load();
    await acc.setup();
    await acc.updateCloudCapabilitiesAndSettings();
    /** @todo We might want to check if login works */
    acc.updateConfigured(); // No need to await this because it doesn't change the account
    acc.store();
}

/**
 * Create an ID for the upload that is uniq over all accounts
 * @param {CloudFileAccount} account 
 * @param {number} fileId 
 * @returns {string} The ID
 */
function makeUploadId(account, fileId) {
    return `${account.id}_${fileId}`;
}