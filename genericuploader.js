import { UploadStatus } from "./uploadstatus.js";
import { State } from "./uploadstatusconstants.js";
import { allUploadStatus } from "./allUploadStatus.js";
import punycode from "../punycode.js/punycode.es6.js";

export class GenericUploader {

    /**
     * @param {Account} account The account loaded with its data from local storage
     * @param {CloudFile} fileInfo As supplied by Thunderbird in the event
     * @param {string} uploadId An ID for the upload that is unique over all accounts
     */
    constructor(account, fileInfo, uploadId) {
        this.account = account;
        this.uploadId = uploadId;
        this.fileName = fileInfo.name;
        this.fileObject = fileInfo.data;
    }

    /**
     * @returns {Promise<{aborted: boolean, url: string}>} 
     */
    async uploadFile() {
        /* Prepare tracking the status */
        this.uploadStatus = new UploadStatus(this.fileName);
        allUploadStatus.set(this.uploadId, this.uploadStatus);

        await this.sanitizeFilename();

        const fullPath = this.account.storageFolder + "/" + _makeUniqueFolderName(this.fileObject) + "/" + this.fileName;

        /* Make sure the file exists in the cloud */
        if (!await this.findOrUploadFile(fullPath)) {
            this.uploadStatus.fail();
            return { aborted: true, };
        }

        /* Make sure the file is shared and get the link */
        const url = await this.findOrCreateShareLink(fullPath);

        if (url) {
            this.uploadStatus.setStatus(State.DONE);
            /* Everything worked, now the status is only needed to show the download password */
            if (!this.uploadStatus.download_password) {
                allUploadStatus.delete(this.uploadId);
            }
            return { url, };
        }
        this.uploadStatus.fail();
        return { aborted: true, };

        /**
         * Make a most probably unique folder name from last modified timestamp and size of fileObject
         * @returns {string} 16 hex characters containing last modified time and size
         */
        function _makeUniqueFolderName(fileObject) {
            return fileObject.lastModified.toString(16).toUpperCase().padStart(11, "0") +
                fileObject.size.toString(16).toUpperCase().padStart(14, "0");
        }
    }

    /**
     * Make sure the file exists in the cloud
     * @param {string} fullPath 
     * @returns {Promise<boolean>} Does the file exist in the cloud?
     */
    async findOrUploadFile(fullPath) {
        /* Check if the same file is in the cloud already */
        const stat = await this.getRemoteFileInfo(fullPath);
        if (stat) {
            if (this.filesMatch(stat, this.fileObject)) {
                return true;
            } else {
                /* There is a different file of the same name in the wrong place */
                this.uploadStatus.setStatus(State.MOVING);
                if (!this.moveConflictingFile()) {
                    return false;
                }
            }
        }

        /* The file is not there, check if we can upload it */
        this.uploadStatus.setStatus(State.CHECK_SIZE);
        if (await this.fileTooBig()) {
            return false;
        }
        this.uploadStatus.setStatus(State.CHECK_SPACE);
        if ((await this.account.freeSpace()) < this.fileObject.size) {
            return false;
        }
        this.uploadStatus.setStatus(State.CREATE_LIBRARY);
        if (!await this.findOrCreateLibrary(this.account.storageLibrary)) {
            return false;
        }
        this.uploadStatus.setStatus(State.CREATE_FOLDER);
        if (!await this.recursivelyCreatePath(fullPath)) {
            return false;
        }

        this.uploadStatus.setStatus(State.UPLOADING);
        if (this.actuallyDoUploadFile(fullPath)) {
            /** @todo update freeSpace hereS */
            this.setMtime(fullPath, this.fileObject.lastModified);
            return true;
        }
        return false;
    }

    /**
     * @param {string} fullPath The full path of the remote file
     * @returns {Promise<boolean>} The path (w/o the file name) exists
     */
    async recursivelyCreatePath(fullPath) {
        const parts = fullPath.split("/").filter(part => !!part);
        for (let i = 2; i < parts.length; i++) {
            const folder = parts.slice(0, i).join("/");
            if (!await this.findOrCreateFolder("/" + folder)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @returns {Promise<boolean>} Is the file bigger than the limit of this account? 
     */
    async fileTooBig() {
        const accountInfo = await messenger.cloudFile.getAccount(this.account.accountId);
        return accountInfo.uploadSizeLimit &&
            accountInfo.uploadSizeLimit > 0 &&
            accountInfo.uploadSizeLimit < this.fileObject.size;
    }

    /**
     * Get an existing or new download link for the file with expiry and download password as configured in account
     * @param {string} fullPath The path of the remote file
     * @returns {Promise<?string>} The download link
     */
    async findOrCreateShareLink(fullPath) {
        this.uploadStatus.setStatus(State.SHARING);

        const expiryDate = this.calculateExpiry();

        const downloadPassword = this.account.getDownloadPassword();
        if (this.account.useGeneratedDlPassword) {
            this.uploadStatus.setPassword(downloadPassword);
        }

        /** @todo use URL here  */
        let url = await this.getFirstSimilarShareOf(fullPath, expiryDate, downloadPassword);
        if (!url) {
            url = this.createNewShareLink(fullPath, expiryDate, downloadPassword);
        }

        if (url) {
            return this.sanitizeUrl(url);
        }

        return null;
    }

    /**
     * Turn "days from now" into an absolute date
     * @returns {?Date} The expiry date of the download or null if no expiry is configured
     */
    calculateExpiry() {
        if (this.account.useExpiry) {
            const d = new Date();
            d.setDate(d.getDate() + this.account.expiryDays);
            return d;
        }
        return null;
    }

    /**
     * Convert a URL into readable form, add necessary parts and remove unnecessary ones
     * @param {?string} url_string A string containing the url 
     * @returns {?string} The converted url or null if it is not a valid url
     */
    async sanitizeUrl(url_string) {
        let url;

        try {
            url = new URL(url_string);
        } catch (error) {
            return null;
        }

        url.pathname = url.pathname.
            split("/").
            filter(dir => !dir.match(/^\.?$/)).
            join("/");

        return url.href.replace(url.hostname, punycode.toUnicode(url.hostname));
    }
}
