export class Popup {
    /**
     * Show an error
     * @param {string} err_id The id of te error to show a message for, eg. use the status code or error type
     */
    static async error(err_id) {
        this._openPopup(".error_bar",
            browser.i18n.getMessage(`error_${err_id}`, Array.from(arguments).slice(1)) ||
            // No message for this error, show the default one
            browser.i18n.getMessage('error_0'));
    }

    /**
     * Show a warning
     * @param {string} messageId The id of the localized string
     */
    static async warn(messageId) {
        this._openPopup(".warning_bar", browser.i18n.getMessage(`warn_${messageId}`, Array.from(arguments).slice(1)));
    }

    /**
     * Show the success popup for 3 seconds
     * 
     * @param {string} [messageId] The id of the message in _locales.
     */
    static async success(messageId = "popup_success") {
        const p = this._openPopup(".success_bar", browser.i18n.getMessage(messageId));
        setTimeout(() => p.remove(), 3000);
    }

    /**
     * Actually opens the popup
     * @param {string} popup The CSS class of the msg_bar to use
     * @param {string} message The message to display
     */
    static _openPopup(popup, message) {
        const new_box = msg_bar_templates.querySelector(popup).cloneNode(true);
        new_box.querySelector(".popup_message").textContent = message;
        new_box.querySelector(".msg_bar_close_button").onclick = this.close;
        return msg_container.appendChild(new_box);
    }

    /**
     * Closes the parent of the close button that has been clicked
     */
    static close() {
        this.parentElement.remove();
    }

    /**
     * Close all popups that might be open
     */
    static async clear() {
        while (msg_container.firstChild) {
            msg_container.firstChild.remove();
        }
    }

    /**
     * Is the popup area empty (no popup currently visible)
     * @returns {boolean}
     */
    static empty() {
        return !Boolean(msg_container.firstChild);
    }
}

/* Use the automatic globals defined by the ids in management.html */
/* globals msg_bar_templates, msg_container */
