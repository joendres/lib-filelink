import { generatePassword } from "./generatepassword.js";

export class GenericAccount {

    /**
     * @param {string} accountId The id as supplied by Thunderbird
     */
    constructor(accountId) {
        this.accountId = accountId;
    }

    /**
     * Load account state from configuration storage
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async load() {
        const id = this.accountId;
        const accountInfo = await browser.storage.local.get(id);
        for (const key in accountInfo[id]) {
            this[key] = accountInfo[id][key];
        }
        return this;
    }

    /**
     * Store the current values of all properties in the local browser storage
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async store() {
        browser.storage.local.set({ [this.accountId]: this, });
        return this;
    }

    /**
     * Initialize a new account before it is shown in the management page for the first time, eg. set default values
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async setupNewAccount() {
        this.storageLibrary = "no library";
        this.storageFolder = "/";
        this.useNoDlPassword = true;

        return this;
    }

    /**
     * Check if the account has every configuration it needs to upload a file
     * @returns {boolean} True if the account is fully configured
     */
    accountComplete() {
        return !!this.serverUrl && !!this.storageFolder && !!this.storageLibrary;
    }

    /**
     * Check if the account is configured and usable and set the flag in Thunderbird accordingly
     * @returns {Account} The object itself for chaining
     */
    updateConfigured() {
        messenger.cloudFile.updateAccount(this.accountId, {
            configured: this.accountComplete(),
        });
        return this;
    }

    /**
     * Clean up when an account is deleted
     */
    async deleteAccount() {
        browser.storage.local.remove(this.accountId);
    }

    /**
     * @returns {Promise<number>}
     */
    async freeSpace() {
        /** @todo where to update from cloud? */
        const accountInfo = await messenger.cloudFile.getAccount(this.accountId);
        return (accountInfo.spaceRemaining && accountInfo.spaceRemaining >= 0) ? accountInfo.spaceRemaining : Infinity;
    }

    /**
     * Get a password for use as a download password. As a side effect if a password is generated, it's stored in the download status.
     * @returns {?string} A password or null if no download passwords configured
     */
    getDownloadPassword() {
        if (this.oneDLPassword && !!this.downloadPassword) {
            return this.downloadPassword;
        } else if (this.useGeneratedDlPassword) {
            const pw = generatePassword(16);
            return pw;
        }
        return null;
    }
}
